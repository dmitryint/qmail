#!/bin/sh

tcprules /tmp/tcp.smtp.cdb /tmp/tcp.smtp.tmp < ${QMAIL_HOME}/etc/tcp.smtp

QMAILDUID=$(id -u qmaild)
NOFILESGID=$(id -g qmaild)
MAXSMTPD=$(cat ${QMAIL_HOME}/control/concurrencyincoming)
LOCAL=$(head -1 ${QMAIL_HOME}/control/me)

if [ -z "\$QMAILDUID" -o -z "\$NOFILESGID" -o -z "\$MAXSMTPD" -o -z "\$LOCAL" ]; then
    echo QMAILDUID, NOFILESGID, MAXSMTPD, or LOCAL is unset
    exit 1
fi

if [ ! -f ${QMAIL_HOME}/control/rcpthosts ]; then
    echo "No ${QMAIL_HOME}/control/rcpthosts!"
    echo "Refusing to start SMTP listener because it'll create an open relay"
    exit 1
fi
exec /usr/local/bin/tcpserver -v -R -l "\$LOCAL" -x /tmp/tcp.smtp.cdb -c "\$MAXSMTPD" \
-u "\$QMAILDUID" -g "\$NOFILESGID" 0 8025 ${QMAIL_HOME}/bin/qmail-smtpd 2>&1
EOF
