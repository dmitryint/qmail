FROM alpine:3.5

ENV QMAIL_HOME "/var/qmail"
ENV PATH /var/qmail/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

RUN adduser qmaild -u 2000 -g nofiles -h ${QMAIL_HOME} -s /sbin/nologin -D \
    && adduser alias -u 2001 -g nofiles -h ${QMAIL_HOME}/alias -s /sbin/nologin -D \
    && adduser qmaill -u 2002 -g nofiles -h ${QMAIL_HOME} -s /sbin/nologin -D \
    && adduser qmailp -u 2003 -g nofiles -h ${QMAIL_HOME} -s /sbin/nologin -D \
    && addgroup -g 2500 qmail \
    && adduser qmailq -u 2004 -g qmail -h ${QMAIL_HOME} -s /sbin/nologin -D \
    && adduser qmailr -u 2005 -g qmail -h ${QMAIL_HOME} -s /sbin/nologin -D \
    && adduser qmails -u 2006 -g qmail -h ${QMAIL_HOME} -s /sbin/nologin -D

ADD ./build.tar.gz /
COPY ./qmail /var/qmail
VOLUME /var/qmail/queue
EXPOSE 8025
USER qmaild

CMD exec /var/qmail/supervise/qmail-smtpd/run